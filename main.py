import json
import logging
import os
import stat
import subprocess
import sys
from collections import defaultdict
from typing import Dict, List

import treefiles as tf
import treefiles.oar as oar


def main():
    out_dir = tf.Tree.new(__file__, "generated").dump(clean=True)
    out_dir.file(
        "start_oar.sh",
        fifo="pipe_file.fifo",
        notify="notify_exec.sh",
        array="array_args.txt",
        runme="runme.sh",
        oar="oarsub_res.txt",
    )

    nb_jobs = 10
    log.info(f"Starting array of size {nb_jobs}")

    # Dump notify script
    notify_script = NOTIFY_SCRIPT.format(fifo_file=out_dir.fifo)
    dump_exe(out_dir.notify, notify_script)
    notif = oar.NotifyOar(out_dir.notify).exec

    # Create array file
    data = defaultdict(list)
    for i in range(nb_jobs):
        data["simu_dir"].append(f"d_{i}")
    dump_data(out_dir.array, data)

    # Dump runme file
    runme_script = RUNME_SCRIPT.format(
        activate=tf.Tree(sys.executable).p.path("activate"),
        python_job=out_dir.p.path("job.py"),
    )
    dump_exe(out_dir.runme, runme_script)

    # Create oar command
    cmd = oar.start_oar(
        out_dir.runme,
        logs_dir=out_dir.dir("logs").dump(),
        wall_time=oar.walltime(minutes=1),
        core=1,
        queue=oar.Queue.BESTEFFORT,
        array_fname=out_dir.array,
        notify=notif,
        do_run=False,
    )

    # Dump start_oar script
    start_oar_script = START_OAR.format(
        oar_command=" ".join(cmd),
        fifo_file=out_dir.fifo,
        oar_msg_file=out_dir.oar,
        nb_jobs=nb_jobs,
    )
    dump_exe(out_dir.start_oar, start_oar_script)

    # Start oar
    shell_out = subprocess.check_output(out_dir.start_oar)
    log.info(shell_out.decode("utf-8").strip())

    log.debug("Resuming program")


def dump_data(fname, data: Dict[str, List[str]]):
    opts = dict(separators=(",", ":"))
    enc_dir = lambda x: f"'{json.dumps(x, **opts)}'"

    _data, n_rows = [], len(next(iter(data.values())))
    for i in range(n_rows):
        row = {}
        for k in data:
            row[k] = data[k][i]
        _data.append(row)

    data1 = [[enc_dir(d)] for d in _data]
    tf.dump_txt(fname, data1)


def dump(fname, content):
    with open(fname, "w") as f:
        f.write(content)


def dump_exe(fname, content):
    dump(fname, content)
    os.chmod(fname, os.stat(fname).st_mode | stat.S_IEXEC)


START_OAR = """
#!/bin/bash

if [ -f {fifo_file} ]; then
    rm {fifo_file}
fi
mkfifo {fifo_file}

{oar_command} > {oar_msg_file} 2>&1

NB_JOBS={nb_jobs}

while true; do
    read reply
    (( NB_JOBS-- ))
    (( NB_JOBS == 0 )) && break
done <> {fifo_file}

echo "{nb_jobs} jobs done!"
""".strip()


NOTIFY_SCRIPT = """
#!/bin/bash

echo "$*" > {fifo_file}
""".strip()


RUNME_SCRIPT = """
#!/bin/bash

source /etc/profile.d/modules.sh
module load conda/2020.11-python3.8

source {activate}

python {python_job} $1
""".strip()


log = logging.getLogger(__name__)


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    # log = tf.get_logger()
    log = tf.get_logger(default=False, handlers=[tf.stream_csv_handler()])

    main()
