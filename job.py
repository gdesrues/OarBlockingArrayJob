import json
import logging
import sys
from time import sleep

import treefiles as tf


def main():
    log.info(f"I am in {simu_dir.abs()}")

    if "d_2" in simu_dir.abs():
        sleep(30)
        log.info("Finished sleeping")


log = logging.getLogger(__name__)

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    log = tf.get_logger(default=False, handlers=[tf.stream_csv_handler()])

    msg = f"you forgot command line arguments for the job: {sys.argv}"
    assert len(sys.argv) > 1, msg
    args = json.loads(sys.argv[1])

    simu_dir = tf.Tree(args["simu_dir"])
    main()
