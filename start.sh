#!/bin/bash

source /etc/profile.d/modules.sh
module load conda/2020.11-python3.8

CUR=$(dirname "$(realpath $0)")
ROOT="$CUR"

MAIN_LOGS="$CUR/logs"
if [ -d $MAIN_LOGS ]; then
	rm -r $MAIN_LOGS
fi
mkdir -p $MAIN_LOGS

source $ROOT/venv/bin/activate

nohup /usr/bin/time -o $MAIN_LOGS/time.txt python $CUR/main.py > $MAIN_LOGS/main.stdout 2> $MAIN_LOGS/main.stderr &

echo "Program 'main.py' started, run 'ps aux | grep main.py' to check"
